console.log('Hello World!');

// [ SECTION ] Syntax, Statements and Comments
	// JS Statements are usually end with semicolon (;)

	//  [ SUB-SECTION ] Types of Comments
		
		/* 
		There are two types of comments:
			1. The single-line comment denoted by two slashes "//".
			2. The multi-line comment denoted by a slash and asterisk 
		*/

// [ SECTION ] Variables
	
	// It is used to contain data.

	// [ SUB-SECTION ] Declaring Variables
		// Declaring Variables - tells our devices that a variable name is created and is ready to store data
		
		// Syntax
			// let/const variableName;
	
	let myVariable;

	console.log(myVariable);

	let hello;
	console.log(hello);

	// Variables must be declared first before they are used.

	// [ SUB-SECTION ] Declaring and Initializing Variables
		// Initializing Variables - the instance when a variable is given it's initial/starting value

		// Syntax
			//let/const variableName = value;

	let productName = "desktop computer";
	console.log(productName);

	let productPrice = 18_999;
	console.log(productPrice);

	const interest = 3.539;
	console.log(interest);

	// [ SUB-SECTION ] Reassigning Variable Values
		// Reassigning a variable means changing it's initial or previous values into another value

		// Syntax
			// variableName = newValue

	productName = "Laptop";
	console.log(productName);

	// Values of const or constants cannot be changed and will simply return an error
	//interest = 3.789;
	//console.log(interest);

	// [ SUB-SECTION ] Reassigning Variables vs. Initializing Variables 
		// Declares a variable first
	let supplier;

		// Initialization is done after the variable has been declared
		// This is considered as initilization because it is the first time that a value have been assign to a varible
	supplier = "John Smith Tradings";
	console.log(supplier);

		//This is considered as reassignment because it's initial was already declared
	supplier = "Zuitt Store";
	console.log(supplier);

// [ SECTION ] var vs let/const

	a = 5;
	console.log(a);
	var a;

// [ SECTION ] let/const local/global scope
	// Scope essentially means where these variables are available for use
	// A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces ia a block.

	let outerVariable = 'hellows';

		{
			let innerVariable = 'vibranium';
			console.log(outerVariable);
			console.log(innerVariable);
		};

	console.log(outerVariable);
	//console.log(innerVariable); // innerVariable is not defined

// [ SECTION ] Multiple Variable Declarations
	// Multiple variables may be declated in one line

	let productCode = 'DC017';
	const productBrand = 'Dell';
	console.log(productCode, productBrand);

// [ SECTION ] Data Types

	// [ SUB-SECTION ] Strings
		// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
		// Strings in JavaScript can be written using either a single ('') or a double ("") quote

	let country =  'Philippines';
	let province = "Metro Manila";

		// Concatenating Strings
			// Multiple string values can be combined to create a single string using the "+" symbol

		let fullAddress = province + ", " + country;
		console.log(fullAddress);

		let greeting = 'I live in the ' + country;
		console.log(greeting);

		// The escape chracter (\) in strings in combination with other characters can produce different effects
		// "\n" refers to creating a new line in between text

			let mailAddress = "Metro Manila\n\nPhilippines";
			console.log(mailAddress);

		// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character

			let message = "John's employees went home early";
			console.log(message);
			message = 'John\'s employees went home early';
			console.log(message);

	// [ SUB-SECTION ] Numbers
		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Number/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combining text and strings
		console.log("John's grade last quarter is " + grade);

	// [ SUB-SECTION ] Boolean
		// Boolean values are normally used to store values relating to the state of certain things
		// This will be useful in furthur discussion about creating logic

		let isMarried = false;
		let inGoodConduct = true;
		console.log('isMarried: ' + isMarried);
		console.log('isGoodConduct: ' + inGoodConduct);

	// [ SUB-SECTION ] Arrays
		// Arrays are special kind of data type that's used to store multiple values

		// Syntax
			// let/const arrayName = [ elementA, elementB, elementC, ....]

		let grades = [ 98.7, 93.4, 98.0, 96.7 ];
		console.log(grades);

		// Different Data Types
			// Storing different data types inside an array is not recommended because it will not make any sense to in the context of programming

			let details = [ "John", "Smith", 32, true ];
			console.log(details);

	// [ SUB-SECTION ] Objects
		// Objects are another special kind of data type that's used to mimic real world object/items

		// Syntax
				// let/const objectName = {
				// 	propertyA: value,	
				// 	propertyB: value	
				// }

			let person = {
				fullName: 'Juan Dela Cruz',
				age: 35,
				isMarried: false,
				contact: [ "+630987654321", "02899012345"],
				address: {
					houseNumber: '345',
					city: 'Manila'
				}
			}

			console.log(person);


		let myGrades = { 
				firstGrading: 98.7, 
				secondGrading: 92.1, 
				thirdGrading: 90.2, 
				fourthGrading: 94.6 
		};

		console.log(myGrades);


// Mini-Activity:

// Create a "product" object that contains the previously discussed data types.

		let product1 = {
			name: "Laptop Table",
			price: 799.00,
			description: "A small wooden laptop table perfect for watching late night movies.",
			colors: ["black", "baby pink", "baby blue", "green"],
			supplier: {
				name: "Woodworks Inc.",
				address: "749 Juan Luna St., Metro Manila"
			},
			isAvailable: true
		};


		console.log(product1);

		// [Sub-Section] Null
		// It is used to intentionally express the absence of a value in a variable declaration.
		// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified.
		let spouse = null;
		// Using null compared to 0 value and an empty string is much better for readability.
		// null is also considered as a data type of it's own 0 which is a data type of a number and single quotes which are  data type of a string.
		let myNumber = 0;
		let myString = '';

		// [Sub-Section] Undefined
		// Represents the state of a variable that has been declared but without an assigned value;
		let fullName;
		console.log(fullName);

		// Undefined vs Null
		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
		// null means that a variable was created and was assigned a value that does not hold any value/amount.
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing.
		let varA = null;
		console.log(varA);

		// For undefined, this is normally caused by developers creating variables that have no value/data associated with them.
		// This occurs when the value of a variable is still unknown.
		let varB;
		console.log(varB);

